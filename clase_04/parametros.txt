program {
	Cuadradito(Rojo)				# uso -> Rojo = argumento
}

procedure Cuadradito(color) {		# definicion -> color = parametro
	repeat(2) {
		Poner(color)
		Mover(Este)
		Poner(color)
		Mover(Oeste)
		Mover(Norte)
	}
	Mover(Sur)
	Mover(Sur)
}

#-

program {
	MoverNorte(3)
	MoverNorte(8)
	MoverNorte(42)
}

procedure MoverNorte(numero) {
	repeat(numero) {
		Mover(Norte)
	}
}

#-

mas de un parametro: separar con coma
