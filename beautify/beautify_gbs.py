import sys

# constants
CHANGE = {
    ": = ": " := ",
    "- > ": " -> ",
    "\n": "",
}


def process_file(f):
    for line in f:
        for key in CHANGE:
            line = line.replace(key, CHANGE[key])
        print(line.rstrip())


def open_n_process(file_name):
    try:
        with open(file_name) as f:
            process_file(f)
    except EnvironmentError:
        print("beautify_gbs: ERROR trying to open the file " + file_name)


def main():
    if (len(sys.argv) < 2):
        print("use: python[3] beautify_gbs.py <input_file> [> <output_file>]")
    else:
        open_n_process(sys.argv[1])

if __name__ == '__main__':
    main()
