interactive program {
	K_ENTER -> {
		MoverSeguro(Este)
	}
	_ -> {}
}

procedure MoverSeguro(dir) {
	if (puedeMover(dir)) {
		Mover(dir)
	}
}
