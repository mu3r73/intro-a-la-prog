# definicion de una funcion
                    # vv parametros
function hayBolitaAl(dir, col) {
    # opera sobre una 'fotocopia' del tablero,
    # que se descarta al salir de la funcion
    Mover(dir)
    return(hayBolitas(col))     # tiene que estar en la ultima linea de la funcion
}


program {
    # uso de una funcion
                    # vv argumentos
    if (hayBolitaAl(Este, Rojo)) {
        Mover(Norte)
    } else {
        Mover(Este)
    }
}
