Expresion - cualquier cosa que puedo escribir en gobstones, que cuando lo evalua genera un valor
    ej: hayBolitas(Rojo)
        hayBolitas(Rojo) && puedeMover(Norte)
        (3 + 8) / 4
        3
        True
        Rojo
        Este

Tipos: categorias o clasificaciones de las expresiones
    booleano (True, False)
    color (Rojo, Verde, Azul, Negro)
    direccion (Este, Norte, Oeste, Sur)
    numero

Valores:
    5
    Este
    True
    Negro

cuando usamos una funcion, es una expresion, que genera un valor de algun tipo
