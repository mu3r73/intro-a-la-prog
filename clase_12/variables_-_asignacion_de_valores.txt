a las variables se les asignan valores

por ejemplo:

function muyUtil() {
    a := 4
    b := 5
    c := a + b
    a := 7
    return(c)
}
# retorna 9

por ejemplo:
# precondicion: tablero vacio
program {
    a := hayBolitas(Rojo)   # False
    Poner(Rojo)             # ok, pero a sigue siendo False
    if (a) {
        Mover(Este)         # nunca pasa porque a sigue siendo False
    }
}
# si hago Poner(Rojo) antes de a := hayBolitas(Rojo), ahi si se mueve
