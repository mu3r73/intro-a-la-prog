# tablero: tetris_1

# las bolitas representan:
# - 1 rojo: secciones que correspondan a piezas que ya tocaron el piso
# - 2+ rojos: secciones de las piezas que aun estan flotando

program {
	DibujarPiezaVertical(10)
}

# dibuja la pieza vertical numero n centrada en la celda actual
# precondicion: las celdas a ocupar estan vacias
procedure DibujarPiezaVertical(n) {
	DibujarPiezaEmpezandoHacia(n, Norte)
}

# dibuja la pieza horizontal numero n centrada en la celda actual
# precondicion: las celdas a ocupar estan vacias
procedure DibujarPiezaHorizontal(n) {
	DibujarPiezaEmpezandoHacia(n, Oeste)
}

# dibuja la pieza numero n centrada en la celda actual, empezando en direccion dir
# precondicion: las celdas a ocupar estan vacias
procedure DibujarPiezaEmpezandoHacia(n, dir) {
	PonerN(n, colorPieza())
	Mover(dir)
	PonerN(n, colorPieza())
	MoverN(2, opuesto(dir))
	PonerN(n, colorPieza())
	Mover(dir)
}

# indica si la pieza puede descender una posicion manteniendo su orientacion
# precondicion: el cabezal se encuentra sobre una pieza flotante
function puedeBajarPieza() {
	puede := False
	if (esHorizontal()) {
		puede := puedeBajarPiezaHorizontal()
	} else	{
		puede := puedeBajarPiezaVertical()
	}
	return(puede)
}

# indica si la pieza puede descender una posicion manteniendo su orientacion
# precondicion: el cabezal se encuentra sobre una pieza flotante horizontal
function puedeBajarPiezaHorizontal() {
	CentrarEnPieza()
	return (puedeBajar()
						&& puedeBajarSeccion(Oeste)
						&& puedeBajarSeccion(Este))
}

# indica si la pieza puede descender una posicion manteniendo su orientacion
# precondicion: el cabezal se encuentra sobre una pieza flotante vertical
function puedeBajarPiezaVertical() {
	CentrarEnPieza()
	return (puedeBajarSeccion(Sur))
}

# indica si la seccion de pieza en la direccion dir puede descender
# precondicion: el cabezal se encuentra en una pieza flotante que continua en direccion dir
function puedeBajarSeccion(dir) {
	Mover(dir)
	return(puedeBajar())
}

# indica si la seccion de pieza en la celda actual puede descender
function puedeBajar() {
	puede := False
	if (puedeMover(Sur)) {
		Mover(Sur)
		puede := not hayBolitas(colorPieza())
	}
	return(puede)
}

# elimina la pieza actual del tablero
# precondicion: el cabezal se encuentra sobre una pieza flotante
# el cabezal debe quedar en la posicion central de la pieza
procedure BorrarPieza() {
	CentrarEnPieza()
	if (esHorizontal()) {
		BorrarPiezaQueEmpiezaHacia(Este)
	} else {
		BorrarPiezaQueEmpiezaHacia(Norte)
	}
}

# elimina la pieza actual del tablero
# precondicion: el cabezal se encuentra centrado en una pieza flotante
procedure BorrarPiezaQueEmpiezaHacia(dir) {
	SacarTodas(colorPieza())
	Mover(dir)
	SacarTodas(colorPieza())
	MoverN(2, opuesto(dir))
	SacarTodas(colorPieza())
	Mover(dir)
}

# indica si la pieza esta en posicion horizontal
# precondicion: el cabezal se encuentra sobre una pieza flotante
function esHorizontal() {
	return(esPiezaHorizontal())
}

# mueve el cabezal para centrarlo en la pieza actual
# precondicion: el cabezal se encuentra sobre una pieza flotante
procedure CentrarEnPieza() {
	if (not centradoEnPieza() && esPiezaHorizontal()) {
		CentrarEnPiezaHorizontal()
	}
	if (not centradoEnPieza() && esPiezaVertical()) {
		CentrarEnPiezaVertical()
	}
}

# mueve el cabezal para centrarlo en la pieza actual
# precondicion: el cabezal se encuentra sobre una pieza flotante orientada horizontalmente
procedure CentrarEnPiezaHorizontal() {
	AlejarDelBorde(Este)
	AlejarDelBorde(Oeste)
}

# mueve el cabezal para centrarlo en la pieza actual
# precondicion: el cabezal se encuentra sobre una pieza flotante orientada verticalmente
procedure CentrarEnPiezaVertical() {
	AlejarDelBorde(Norte)
	AlejarDelBorde(Sur)
}

# si el cabezal esta en el borde de una pieza en direccion dir,
# lo mueve 1 celda en direccion opuesta
procedure AlejarDelBorde(dir) {
	if (not continuaPiezaHacia(dir)) {
		Mover(opuesto(dir))
	}
}

# indica si la pieza actual esta orientada verticalmente
# precondicion: el cabezal se encuentra sobre una pieza flotante
function esPiezaVertical() {
	return(continuaPiezaHacia(Norte) || continuaPiezaHacia(Sur))
}

# indica si la pieza actual esta orientada horizontalmente
# precondicion: el cabezal se encuentra sobre una pieza flotante
function esPiezaHorizontal() {
	return(continuaPiezaHacia(Este) || continuaPiezaHacia(Oeste))
}

# indica si el cabezal se encuentra en la celda central de un pieza
function centradoEnPieza() {
	return(haySeccion()
					&& (centradoEnPiezaVertical() || centradoEnPiezaHorizontal()))
}

# indica si el cabezal se encuentra en la celda central de un pieza vertical
# precondicion: el cabezal se encuentra sobre una pieza flotante
function centradoEnPiezaVertical() {
	return(continuaPiezaHacia(Norte) && continuaPiezaHacia(Sur))
}

# indica si el cabezal se encuentra en la celda central de un pieza horizontal
# precondicion: el cabezal se encuentra sobre una pieza flotante
function centradoEnPiezaHorizontal() {
	return (continuaPiezaHacia(Este) && continuaPiezaHacia(Oeste))
}

# indica si la pieza continua hacia la direccion dir
# precondicion: el cabezal se encuentra sobre una pieza flotante
function continuaPiezaHacia(dir) {
	return(haySeccionAl(dir)
					&& (numeroSeccionAl(dir) == numeroSeccion()))
}

# indica el numero de la seccion en la celda lindante en direccion dir
# precondicion: hay seccion en la celda lindante en direccion dir
function numeroSeccionAl(dir) {
	Mover(dir)
	return(numeroSeccion())
}

# indica si hay una seccion en la celda lindante en direccion dir
function haySeccionAl(dir) {
	resultado := False
	if (puedeMover(dir)) {
		Mover(dir)
		resultado := haySeccion()
	}
	return(resultado)
}

# indica si hay una seccion en la celda actual
function haySeccion() {
	return(hayBolitas(colorPieza()))
}

# indica si la seccion corresponde a una pieza flotante
# precondicion: el cabezal se encuentra sobre una seccion
function estaFlotando() {
	return(numeroSeccion() > 1)
}

# retorna el numero de la seccion actual
# precondicion: el cabezal se encuentra sobre una seccion
function numeroSeccion() {
	return(nroBolitas(colorPieza()))
}

function colorPieza() {
	return(Rojo)
}

function numeroPiso() {
	return(1)
}
