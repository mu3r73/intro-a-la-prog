# tablero: hongos

/*
las bolitas representan:
- negro, verde: manchas de hongos (cada mancha es de un solo color)
- rojo: producto para sacar manchas de hongos
*/

program {
  BorrarManchaSiTieneAntihongos()
}

/*
Proposito: si la mancha actual tiene antihongos, la borra, y borra tambien los antihongos
Precondicion: la celda actual esta en una mancha
*/
procedure BorrarManchaSiTieneAntihongos() {
	if (tieneAntihongosLaManchaActual()) {
		MarcarMancha()
		BorrarLoMarcado()
		DesmarcarTodo()
	}
}

/*
Proposito: borra mancha y antihongos de la mancha marcada
Precondicion: hay una mancha marcada
*/
procedure BorrarLoMarcado() {
	IrAlOrigen()
	BorrarSiEstaMarcada()
	while (haySiguiente()) {
		IrAlSiguiente()
		BorrarSiEstaMarcada()
	}
}

/*
Proposito: si la celda actual esta marcada, borra mancha y antihongos
*/
procedure BorrarSiEstaMarcada() {
	if (tieneMarca()) {
		BorrarHongos()
		BorrarAntihongos()
	}
}

/*
Proposito: borra hongos de la celda actual
*/
procedure BorrarHongos() {
	SacarTodas(colorDeHongo())
}

/*
Proposito: borra antihongos de la celda actual
*/
procedure BorrarAntihongos() {
	SacarTodas(Rojo)
}

/*
Proposito: indica si todas las celdas de la mancha actual tienen antihongos
Precondicion: la celda actual esta en una mancha
*/
function tieneAntihongosLaManchaActual() {
	MarcarMancha()
	IrAlOrigen()
	while (haySiguiente() && not hayMarcaSinAntihongos()) {
		IrAlSiguiente()
	}
	return(not hayMarcaSinAntihongos())
}

/*
Proposito: indica si la celda actual esta marcada y no tiene antihongos
*/
function hayMarcaSinAntihongos() {
	return(tieneMarca() && not tieneAntihongos())
}

/*
Proposito: indica si la celda actual tiene antihongos
*/
function tieneAntihongos() {
	return(hayBolitas(Rojo))
}

/*
Proposito: pone una bolita roja en cada celda de la mancha actual
Precondicion: la celda actual esta en una mancha
*/
procedure AplicarAntihongosAManchaActual() {
	MarcarMancha()
	AplicarAntihongosAMarcadas()
	DesmarcarTodo()
}

/*
Proposito: pone una bolita roja en cada celda marcada
*/
procedure AplicarAntihongosAMarcadas() {
	IrAlOrigen()
	AplicarAntihongosSiEstaMarcada()
	while (haySiguiente()) {
		IrAlSiguiente()
		AplicarAntihongosSiEstaMarcada()
	}
}

/*
Proposito: si la celda actual esta marcada, le agrega una bolita roja
*/
procedure AplicarAntihongosSiEstaMarcada() {
	if (tieneMarca()) {
		Poner(Rojo)
	}
}

/*
Proposito: saca las bolitas que se pusieron para marcar
*/
procedure DesmarcarTodo() {
  IrAlOrigen()
  DesmarcarMarcada()
  while(haySiguiente()) {
    IrAlSiguiente()
    DesmarcarMarcada()
  }
}

/*
Proposito: si la celda actual esta marcada, la desmarca
*/
procedure DesmarcarMarcada() {
  if (tieneMarca()) {
    SacarTodas(Azul)
  }
}

/*
Proposito: marca la mancha donde esta la celda actual
Precondicion: la celda actual tiene hongos de algun tipo
*/
procedure MarcarMancha() {
	color := colorDeHongo()
  Marcar()
  while (hayCeldaMarcable(color)) {
    BuscarCeldaMarcable(color)
    Marcar()
  }
}

/*
Proposito: indica el color del hongo en la celda actual
Precondicion: hay hongo (de un solo color) en la celda actual
*/
function colorDeHongo() {
	color := Verde
	if (hayBolitas(Negro)) {
		color := Negro
	}
	return(color)
}

/*
Proposito: indica si queda alguna celda para marcar en el tablero
*/
function hayCeldaMarcable(color) {
  BuscarCeldaMarcable(color)
  return(esCeldaMarcable(color))
}

/*
Proposito: ubica el cabezal en una celda marcable
*/
procedure BuscarCeldaMarcable(color) {
  IrAlOrigen()
  while (haySiguiente() && not esCeldaMarcable(color)) {
    IrAlSiguiente()
  }
}

/*
Proposito: indica si la celda actual es una "candidata"
(= no esta marcada, es parte de una region, y tiene una vecina marcada)
*/
function esCeldaMarcable(color) {
  return(not tieneMarca() && tieneHongoDeColor(color) && tieneVecinaMarcada())
}

/*
Proposito: indica si la celda actual es parte de una mancha del color indicado
*/
function tieneHongoDeColor(color) {
  return(hayBolitas(color))
}

/*
Proposito: indica si la celda actual tiene celda vecina marcada
*/
function tieneVecinaMarcada() {
  return(hayVecinaMarcadaHacia(Norte)
        || hayVecinaMarcadaHacia(Este)
        || hayVecinaMarcadaHacia(Sur)
        || hayVecinaMarcadaHacia(Oeste))
}

/*
Proposito: indica si la celda en direccion dir esta marcada
  (si no hay celda adyacente en la direccion dir, o si hay adyacente pero no tiene marca, retorna False)
*/
function hayVecinaMarcadaHacia(dir) {
  resultado := False
  if (puedeMover(dir)) {
    Mover(dir)
    resultado := tieneMarca()
  }
  return(resultado)
}

/*
Proposito: indica si la celda actual esta marcada (si tiene una bolita azul)
*/
function tieneMarca() {
  return(nroBolitas(Azul) == 1)
}

/*
Proposito: desmarca la celda actual
*/
procedure Desmarcar() {
  SacarTodas(Azul)
}

/*
Proposito: marca la celda actual (con 1 bolita azul)
*/
procedure Marcar() {
  Poner(Azul)
}

/*
Proposito: indica si hay al menos una bolita verde o negra en el tablero
*/
function hayHongos() {
	IrAlOrigen()
	while (haySiguiente() && not tieneHongo()) {
		IrAlSiguiente()
	}
	return(tieneHongo())
}

/*
indica si hay hongos en la celda actual
*/
function tieneHongo() {
	return(hayBolitas(Negro) || hayBolitas(Verde))
}
