# tablero: es_region_vacia_rodeada_de_verde

program {
	return(esRegionVaciaRodeadaDeVerde())
}

/*
Proposito: indica si la celda actual esta en una region vacia, rodeada totalmente de celdas con bolitas verdes
Precondicion: no hay bolitas negras en el tablero
*/
function esRegionVaciaRodeadaDeVerde() {
	MarcarRegionVacia()
	MarcarVecinosDeRegionVacia()
	return(todasLasVecinasMarcadasTienenVerde())
}

/*
Proposito: ​si todas las celdas vecinas marcadas del tablero tienen tambien bolitas verdes
*/
function todasLasVecinasMarcadasTienenVerde() {
	IrAlOrigen()
	while (haySiguiente() && not esVecinaSinVerde()) {
		IrAlSiguiente()
	}
	return(not esVecinaSinVerde())
}

/*
Proposito: retorna True si la celda actual esta marcada como vecina y no tiene bolitas verdes,
    y False en caso contrario
*/
function esVecinaSinVerde() {
	resultado := False
	if (esVecinaMarcada() && not hayBolitas(Verde)) {
		resultado := True
	}
	return(resultado)
}


/*
Proposito: marca todas las celdas vecinas a la region actual
Precondicion: la region actual esta marcada
*/
procedure MarcarVecinosDeRegionVacia() {
	IrAlOrigen()
	MarcarVecinosSiEstaMarcada()
	while (haySiguiente()) {
		IrAlSiguiente()
		MarcarVecinosSiEstaMarcada()
	}
}

/*
Proposito: si la celda actual esta marcada, marca sus vecinas que no forman parte de la region
*/
procedure MarcarVecinosSiEstaMarcada() {
	if (tieneMarca()) {
		MarcarVecinos()
	}
}

/*
Proposito: marca todas las celdas vecinas a la celda actual que no forman parte de la region
Precondicion: la celda actual esta marcada
*/
procedure MarcarVecinos() {
	MarcarVecinoHacia(Norte)
	MarcarVecinoHacia(Este)
	MarcarVecinoHacia(Sur)
	MarcarVecinoHacia(Oeste)
}

/*
Proposito: marca la celda vecina a la celda actual en direccion dir
*/
procedure MarcarVecinoHacia(dir) {
	if (puedeMover(dir) && not tieneMarcaVecino(dir)) {
		MarcarVecino(dir)
	}
}

/*
Proposito: indica si la celda vecina a la region actual en direccion dir esta marcada
*/
function tieneMarcaVecino(dir) {
	resultado := False
	if (puedeMover(dir)) {
		Mover(dir)
		resultado := tieneMarca() || esVecinaMarcada()
	}
	return(resultado)
}

/*
Proposito: indica si la celda actual esta marcada como vecina
*/
function esVecinaMarcada() {
	return(nroBolitas(Negro) == 2)
}

/*
Proposito: pone marca en la celda vecina a la region actual en direccion dir
Precondicion: hay celda vecina a la direccion actual en direccion dir, y no esta marcada
*/
procedure MarcarVecino(dir) {
	Mover(dir)
	PonerN(2, Negro)
	Mover(opuesto(dir))
}

/*
Proposito: indica si la celda en direccion dir es vecina a la region actual
(= no es 'vacia')
*/
function esVecina(dir) {
  resultado := False
  if (puedeMover(dir)) {
    Mover(dir)
    resultado := not esVacia()
  }
  return(resultado)
}

/*
Proposito: indica si la celda actual esta en una region vacia de al menos 20 celdas
	Si la celda actual no esta vacia, retorna False ​
Precondicion: no hay bolitas negras en el tablero
*/
function esRegionVaciaGrandota() {
	resultado := False
	if (esVacia()) {
		resultado := (tamanioDeRegionVacia() >= 20)
	}
	return(resultado)
}

/*
Proposito: retorna el tama#o (cantidad de celdas), de la region vacia actual
Precondicion: el cabezal esta en una celda vacia
Precondicion general: no hay marcas en el tablero
*/
function tamanioDeRegionVacia() {
  MarcarRegionVacia()
  IrAlOrigen()
  tamanio := nroBolitas(Negro)
  while (haySiguiente()) {
    IrAlSiguiente()
    tamanio := tamanio + nroBolitas(Negro)
  }
  return(tamanio)
}

/*
Proposito: pinta del color indicado la region vacia donde esta la celda actual
Precondicion: la celda actual esta vacia, y no hay bolitas de marca (negras) en el tablero
*/
procedure PintarRegionVacia(col) {
	MarcarRegionVacia()
	PintarMarcadas(col)
	SacarMarcas()
}

/*
Proposito: pinta del color indicado la region marcada
Precondicion: hay una region marcada
*/
procedure PintarMarcadas(col) {
	IrAlOrigen()
	PintarSiHayMarca(col)
	while (haySiguiente()) {
		IrAlSiguiente()
		PintarSiHayMarca(col)
	}
}

/*
Proposito: si la celda actual esta marcada, la pinta con el color indicado
*/
procedure PintarSiHayMarca(col) {
	if (tieneMarca()) {
		Poner(col)
	}
}

/*
Proposito: saca las bolitas que se pusieron para marcar
Precondicion: el cabezal esta en una region vacia marcada
*/
procedure SacarMarcas() {
  IrAlOrigen()
  DesmarcarMarcada()
  while(haySiguiente()) {
    IrAlSiguiente()
    DesmarcarMarcada()
  }
}

/*
Proposito: si la celda actual esta marcada, la desmarca
*/
procedure DesmarcarMarcada() {
  if (tieneMarca()) {
    Desmarcar()
  }
}

/*
Proposito: saca la marca (bolita negra) en la celda actual
Precondicion: la celda actual esta marcada
*/
procedure Desmarcar() {
  SacarTodas(Negro)
}

/*
Proposito: marca la region vacia actual
Precondicion: el cabezal esta en una celda vacia
*/
procedure MarcarRegionVacia() {
  Marcar()
  while (hayCeldaMarcable()) {
    BuscarCeldaMarcable()
    Marcar()
  }
}

/*
Proposito: indica si queda alguna celda para marcar en el tablero
*/
function hayCeldaMarcable() {
  BuscarCeldaMarcable()
  return(esCeldaMarcable())
}

/*
Proposito: ubica el cabezal en una celda marcable
*/
procedure BuscarCeldaMarcable() {
  IrAlOrigen()
  while (haySiguiente() && not esCeldaMarcable()) {
    IrAlSiguiente()
  }
}

/*
Proposito: indica si la celda actual es una "candidata"
(= es vacia, y tiene una vecina marcada)
*/
function esCeldaMarcable() {
  return(not tieneMarca() && esVacia() && tieneVecinaMarcada())
}

/*
Proposito: indica si la celda actual es parte de una region (= es vacia)
*/
function esVacia() {
  return(not hayBolitas(Azul)
#					&& not hayBolitas(Negro)		# puede ser vacia con marca
					&& not hayBolitas(Rojo)
					&& not hayBolitas(Verde))
}

/*
Proposito: indica si la celda actual tiene celda vecina marcada
*/
function tieneVecinaMarcada() {
  return(hayVecinaMarcadaHacia(Norte)
        || hayVecinaMarcadaHacia(Este)
        || hayVecinaMarcadaHacia(Sur)
        || hayVecinaMarcadaHacia(Oeste))
}

/*
Proposito: indica si la celda en direccion dir esta marcada
  (si no hay celda adyacente en la direccion dir, o si hay adyacente pero no tiene marca, retorna False)
*/
function hayVecinaMarcadaHacia(dir) {
  resultado := False
  if (puedeMover(dir)) {
    Mover(dir)
    resultado := tieneMarca()
  }
  return(resultado)
}

/*
Proposito: indica si la celda actual esta marcada (si tiene una bolita negra)
*/
function tieneMarca() {
  return(nroBolitas(Negro) == 1)
}

/*
Proposito: marca la celda actual (con 1 bolita negra)
*/
procedure Marcar() {
  Poner(Negro)
}
