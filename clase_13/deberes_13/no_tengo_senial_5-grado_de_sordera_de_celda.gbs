# tablero: no_tengo_senial

/*
las bolitas representan:
- azul: potencia de antena
- rojo: identificador de antena
- verde: senial de antena
- negro: marca temporal
*/

program {
	return(gradoDeSorderaDeCelda(3))
}

# Ejercicio 5:

/*
Proposito: retorna el grado de sordera de la celda actual con respecto al umbral
  En caso en que la celda actual reciba una se#al superior a umbral, el resultado es 0
  El grado de sordera de una celda esta dado por el maximo grado de sordera en cada una de las cuatro direcciones
Precondicion: el mapa no contiene marcas de se#al ni marcas temporales
*/
function gradoDeSorderaDeCelda(umbral) {
  MarcarTemporalmente()
  MarcarCoberturaTotal()
  IrAMarcaTemporal()
  grado := gradoDeSorderaDeCeldaHacia(Norte, umbral)
  grado := max(grado, gradoDeSorderaDeCeldaHacia(Este, umbral))
  grado := max(grado, gradoDeSorderaDeCeldaHacia(Sur, umbral))
  grado := max(grado, gradoDeSorderaDeCeldaHacia(Oeste, umbral))
  return(grado)
}

/*
Proposito: retorna el valor maximo entre valor1 y valor2
*/
function max(valor1, valor2) {
  resultado := valor1
  if (valor2 > valor1) {
    resultado := valor2
  }
  return(resultado)
}

/*
Proposito: retorna el grado de sordera de la celda con respecto al umbral, en la direccion indicada
  El grado de sordera en una direccion dada dir es la cantidad de celdas consecutivas en esa direccion que reciben una se#al menor a umbral
*/
function gradoDeSorderaDeCeldaHacia(dir, umbral) {
  grado := 0
  while (puedeMover(dir) && esSorda(umbral)) {
    Mover(dir)
    grado := grado + contar1SiEsSorda(umbral)
  }
  return(grado)
}

/*
Proposito: retorna 1 si la celda es sorda, y 0 en caso contrario
*/
function contar1SiEsSorda(umbral) {
  resultado := 0
	if (esSorda(umbral)) {
    resultado := 1
  }
  return(resultado)
}

/*
Proposito: determina si en la region hay al menos una celda prioritaria de acuerdo al umbral,
  donde se asume que la potencia de cada antena esta dada por maxPotencia
Precondicion: que maxPotencia sea la mayor potencia posible para cada antena
Precondicion: no hay marcas de se#al en el tablero
*/
function hayCeldaPrioritaria(maxPotencia, umbral) {
  AumentarPotenciaTotal(maxPotencia)
  MarcarCoberturaTotal()
  IrAlOrigen()
  while (haySiguiente() && not esPrioritaria(umbral)) {
    IrAlSiguiente()
  }
  return(esPrioritaria(umbral))
}
# ^^ la precondicion 'maxPotencia sea la mayor potencia posible' parece ser incorrecta
# (ver ejercicio 7)
# para solucionar esto, deberia mover los MarcarCoberturaTotal y AumentarPotenciaTotal
# a esPrioritaria(umbral), que pasaria a ser esPrioritaria(maxPotencia, umbral),
# y haria mas o menos esto:
# function esPrioritaria(maxPotencia, umbral) {
# 		MarcarPotenciaTotal()
# 		res := esSorda(umbral)
# 		if (res) {
#   	DesmarcarCoberturaTotal() # <- no existe, habria que escribirlo
#   	AumentarPotenciaTotal(maxPotencia)
#   	MarcarCoberturaTotal()
#   	res := esSorda(umbral)
# 		}
#		return(res)
# }

/*
Proposito: aumenta la potencia de todas las antenas del tablero al maximo indicado
*/
procedure AumentarPotenciaTotal(maxPotencia) {
  IrAlOrigen()
  CambiarPotenciaSiHayAntena(maxPotencia)
  while (haySiguiente()) {
    IrAlSiguiente()
    CambiarPotenciaSiHayAntena(maxPotencia)
  }
}

/*
Proposito: si hay una antena en la celda actual, cambia su potencia al valor indicado
*/
procedure CambiarPotenciaSiHayAntena(potencia) {
  if (hayAntena()) {
    CambiarPotencia(potencia)
  }
}

/*
Proposito: cambia la potencia de la antena en la celda actual al valor indicado
Precondicion: hay una antena en la celda actual
*/
procedure CambiarPotencia(potencia) {
  id := idAntena()
  SacarAntena()
  PonerAntena(id, potencia)
}

/*
Proposito: determina la celda actual es prioritaria
  Una celda se dice prioritaria si la senial no puede alcanzar el umbral minimo requerido,
  incluso si la potencia de cada antena se incrementa al maximo
Precondicion: que la potencia de todas las antenas de la region haya sido elevada al maximo
*/
function esPrioritaria(umbral) {
  return(esSorda(umbral))
}

/*
Proposito: retorna True si hay al menos una celda en la region,
	tal que la se#al total recibida es menor a umbral
Precondicion: el mapa no tiene marcas de se#al
*/
function hayCeldaSorda(umbral) {
  MarcarCoberturaTotal()
  IrAlOrigen()
  while (haySiguiente() && not esSorda(umbral)) {
    IrAlSiguiente()
  }
  return(esSorda(umbral))
}

function esSorda(umbral) {
  return(marcasDeSenial() < umbral)
}


/*
Proposito: acumula las se#ales de todas las antenas de la region 
Precondicion: el tablero no tiene marcas de senial
*/
procedure MarcarCoberturaTotal() {
  IrAlOrigen()
  MarcarCoberturaSiHayAntena()
  while (haySiguiente()) {
    IrAlSiguiente()
    MarcarCoberturaSiHayAntena()
  }
}

procedure MarcarCoberturaSiHayAntena() {
  if (hayAntena()) {
    MarcarCobertura()
  }
}

/*
Proposito: acumula la se#al en cada celda alcanzada por la antena
  que se encuentra debajo del cabezal
Precondicion: hay una antena en la celda actual.
*/
procedure MarcarCobertura() {
  AgregarSenial(potenciaAntena())
  MarcarCoberturaHacia(Norte)
  MarcarCoberturaHacia(Este)
  MarcarCoberturaHacia(Sur)
  MarcarCoberturaHacia(Oeste)
}

procedure MarcarCoberturaHacia(dir) {
  potencia := potenciaAntena()
  id := idAntena()
  while (puedeMover(dir) && (potencia > 1)) {
    Mover(dir)
    potencia := potencia - 1
    AgregarSenial(potencia)
  }
  VolverAAntena(id, opuesto(dir))
}

/*
Proposito: vuelve a la antena con identificacion id, en la direccion dir
Precondicion: existe una antena con identificacion id en la direccion dir
*/
procedure VolverAAntena(id, dir) {
	while (idAntena() /= id) {
		Mover(dir)
	}
}

/*
Proposito: retorna el identificador de la antena bajo el cabezal
*/
function idAntena() {
	return(nroBolitas(Rojo))
}

/*
Proposito: retorna la potencia de la antena bajo el cabezal
  (cant de bolitas azules)
*/
function potenciaAntena() {
  return(nroBolitas(Azul))
}

/*
Proposito: agrega (suma) una se#al de la potencia indicada a la celda actual
*/
procedure AgregarSenial(potencia) {
  PonerN(potencia, Verde)
}

/*
Proposito: indica si hay una antena en la celda actual
*/
function hayAntena() {
  return(hayBolitas(Rojo))
}

/*
Proposito: retorna la cantidad de se#al marcada con AgregarSenial que hay en la celda actual
*/
function marcasDeSenial() {
	return(nroBolitas(Verde))
}

/*
Proposito: agrega una antena en la celda actual
*/
procedure PonerAntena(id, potencia) {
	PonerN(id, Rojo)
	PonerN(potencia, Azul)
}

/*
Proposito: elimina la antena en la celda actual
*/
procedure SacarAntena() {
	SacarTodas(Rojo)
	SacarTodas(Azul)
}

/*
Proposito: marca una celda en forma temporal
*/
procedure MarcarTemporalmente() {
	Poner(Negro)
}

/*
Proposito: posiciona el cabezal en la cualquier celda marcada en forma temporal
Precondicion: hay marca temporal
*/
procedure IrAMarcaTemporal() {
	IrAlOrigen()
	while (not hayMarcaTemporal()) {
		IrAlSiguiente()
	}
}

function hayMarcaTemporal() {
	return(hayBolitas(Negro))
}
