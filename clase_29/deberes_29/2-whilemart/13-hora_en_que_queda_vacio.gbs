type Cliente is record {
  field horaIngresoCola # Numero
  field cantProductos   # Numero
}

type Caja is record {
  field numero            # Numero
  field clientesEsperando # Lista[Cliente]
}

type Super is record {
  field horaActual        # Numero
  field lineaDeCajas      # Lista[Caja]
  field clientesComprando # Lista[Cliente]
}

program {
	cliente1 := Cliente(horaIngresoCola <- 0, cantProductos <- 5)
	cliente2 := Cliente(horaIngresoCola <- 0, cantProductos <- 7)
	cliente3 := Cliente(horaIngresoCola <- 550, cantProductos <- 10)
	cliente4 := Cliente(horaIngresoCola <- 549, cantProductos <- 3)
	cliente5 := Cliente(horaIngresoCola <- 551, cantProductos <- 10)
	cliente6 := Cliente(horaIngresoCola <- 555, cantProductos <- 6)
	cliente7 := Cliente(horaIngresoCola <- 570, cantProductos <- 7)

	caja1 := Caja(numero <- 1, clientesEsperando <- [cliente4, cliente5])
	caja2 := Caja(numero <- 2, clientesEsperando <- [cliente3])
	caja3 := Caja(numero <- 3, clientesEsperando <- [cliente1, cliente1, cliente1, cliente1])

	whileMart := Super(horaActual <- 555, lineaDeCajas <- [caja1, caja3, caja2], clientesComprando <- [cliente6, cliente7])

	return(horaEnQueQuedaVacio(whileMart))
}

# indica la hora en la que el supermercado queda completamente vacıo
function horaEnQueQuedaVacio(whileMart) {
	super := whileMart
	while(not supermercadoVacio(super)) {
		super := pasoDelTiempo(super)
	}
	return(horaActual(super))
}

# indica si el supermercado está completamente vacio
# = si no hay clientes comprando y no hay clientes esperando en ninguna caja
function supermercadoVacio(whileMart) {
	return(not hayClientesComprando(whileMart)
					&& not hayClientesEnAlgunaCaja(whileMart))
}

# indica si en el super hay clientes realizando compras
function hayClientesComprando(whileMart) {
	return(tieneAlgo(clientesComprando(whileMart)))
}

# indica si en el super hay clientes en cola en alguna caja
function hayClientesEnAlgunaCaja(whileMart) {
	lcajas := lineaDeCajas(whileMart)
	while (tieneAlgo(lcajas) && not tieneClientes(head(lcajas))) {
		lcajas := tail(lcajas)
	}
	return(tieneAlgo(lcajas))
}

# retorna el supermercado despues de que pase una unidad de tiempo:
# - los clientes que terminaron sus compras en la hora actual pasan a las cajas
# - procesa un producto en cada caja de la linea de cajas
# - avanza el reloj una unidad de tiempo
function pasoDelTiempo(whileMart) {
	nuevoSuper := finalDeLasCompras(whileMart)
	nuevaLineaDeCajas := avanzaLineaDeCajas(lineaDeCajas(nuevoSuper))
	return(Super(horaActual <- (horaActual(nuevoSuper) + 1), lineaDeCajas <- nuevaLineaDeCajas, clientesComprando <- clientesComprando(nuevoSuper)))
# por alguna razon, la notacion | no funciona en este caso
#	return(Super(nuevoSuper | horaActual <- (horaActual(whileMart) + 1), lineaDeCajas <- nuevaLineaDeCajas))
}

# retorna el super que se obtiene de hacer que ingresen a alguna cola
# todos los clientes cuya horaIngresoCola coincide con la hora actual del super
function finalDeLasCompras(whileMart) {
	nuevaLineaDeCajas := lineaDeCajas(whileMart)
	clientesEntrandoEnCola := clientesQueIngresanALas(clientesComprando(whileMart), horaActual(whileMart))
	foreach cliente in clientesEntrandoEnCola {
		nuevaLineaDeCajas := ingresaColaCliente(nuevaLineaDeCajas, cliente)
	}
	restoDeLosClientes := clientesQueNoIngresanALas(clientesComprando(whileMart), horaActual(whileMart))
	return(Super(horaActual <- horaActual(whileMart), lineaDeCajas <- nuevaLineaDeCajas, clientesComprando <- restoDeLosClientes))
# por alguna razon, la notacion | no funciona en este caso
#	return(Super(whileMart | lineaDeCajas <- nuevaLineaDeCajas, clientesComprando <- restoDeLosClientes))
}

# retorna la listas de clientes que ingresan a una cola a dicha hora
function clientesQueIngresanALas(clientes, hora) {
	lres := []
	foreach cliente in clientes {
		if (ingresaALas(cliente, hora)) {
			lres := lres ++ [cliente]
		}
	}
	return(lres)
}

# retorna la listas de clientes que NO ingresan a una cola a dicha hora
function clientesQueNoIngresanALas(clientes, hora) {
	lres := []
	foreach cliente in clientes {
		if (not ingresaALas(cliente, hora)) {
			lres := lres ++ [cliente]
		}
	}
	return(lres)
}

# indica si el cliente empieza a hacer cola en una caja a la hora dada
function ingresaALas(cliente, hora) {
	return(horaIngresoCola(cliente) == hora)
}

# proposito: retorna la linea de cajas que se obtiene
# despues de que el cliente comienza a hacer cola;
# el cliente se ubicara en la caja que este menos ocupada
# precondicion: lineaDeCajas tiene por lo menos una caja
function ingresaColaCliente(lineaDeCajas, cliente) {
	caja := cajaMenosOcupada(lineaDeCajas)
	laux := sinElem(caja, lineaDeCajas)
	caja := Caja(caja | clientesEsperando <- (clientesEsperando(caja) ++ [cliente]))
	return(laux ++ [caja])
}

# proposito: retorna la caja de que esta menos ocupada,
# es decir, aquella en la que hay menos clientes haciendo cola
# precondicion: lineaDeCajas tiene por lo menos una caja
function cajaMenosOcupada(lineaDeCajas) {
	menosOcupada := head(lineaDeCajas)
	foreach caja in tail(lineaDeCajas) {
		if (esMenosOcupada(caja, menosOcupada)) {
			menosOcupada := caja
		}
	}
	return(menosOcupada)
}

# proposito: indica si la caja1 esta menos ocupada que la caja2,
# es decir, tiene menos clientes haciendo cola;
# si ambas cajas tienen la misma cantidad de clientes esperando,
# se desempata por menor numero de caja
function esMenosOcupada(caja1, caja2) {
	long1 := longitud(clientesEsperando(caja1))
	long2 := longitud(clientesEsperando(caja2))
	return((long1 < long2)
					|| ((long1 == long2) 
								&& (numero(caja1) < numero(caja2))))
}

# retorna la lınea de cajas que se obtiene
# despues de que todas las cajas procesan un producto
function avanzaLineaDeCajas(lineaDeCajas) {
	nuevaLinea := lineaDeCajas
	foreach caja in lineaDeCajas {
		nuevaLinea := avanzaCaja(nuevaLinea, numero(caja))
	}
	return(nuevaLinea)
}

# proposito: retorna la linea de cajas que se obtiene
# despues de que la caja identificada por nroCaja procesa un producto
# precondicion: hay una caja identificada por nroCaja en la linea
function avanzaCaja(lineaDeCajas, nroCaja) {
	nuevaLinea := lineaDeCajas
	if (tieneClientes(buscarCaja(nuevaLinea, nroCaja))) {
		nuevaLinea := actualizarCajaEnLinea(nuevaLinea, nroCaja)
	} # si la caja encontrada no tiene clientes, la lınea queda tal como estaba
	return(nuevaLinea)
}

# indica si hay clientes esperando en la caja
function tieneClientes(caja) {
	return(tieneAlgo(clientesEsperando(caja)))
}

# proposito: actualiza la caja identificada por nroCaja en la linea de cajas
# precondicion: hay una caja identificada por nroCaja en la linea
function actualizarCajaEnLinea(lineaDeCajas, nroCaja) {
	caja := buscarCaja(lineaDeCajas, nroCaja)
	linea := sinElem(caja, lineaDeCajas)
	return(linea ++ [actualizarCaja(caja)])
}

# proposito: procesa un producto del cliente que esta siendo atendido;
# si ya se procesaron todos los productos de ese cliente, se retira al cliente de la caja
function actualizarCaja(caja) {
	nuevaCaja := caja # inicializacion
	if (tieneProductos(head(clientesEsperando(caja)))) {
		nuevaCaja := Caja(caja | clientesEsperando <- [procesarCliente(head(clientesEsperando(caja)))] ++ tail(clientesEsperando(caja)))
	} else {
		# si ya se procesaron todos los productos del cliente que esta siendo atendido,
		# el cliente se retira de la caja
		nuevaCaja := Caja(caja | clientesEsperando <- tail(clientesEsperando(caja)))
	}
	return(nuevaCaja)
}

# indica si el cliente tiene productos sin procesar
function tieneProductos(cliente) {
	return(cantProductos(cliente) /= 0)
}

# proposito: procesa un producto del cliente dado
# precondicion: el cliente dado tiene al menos un producto para procesar
function procesarCliente(cliente) {
	return(Cliente(horaIngresoCola <- horaIngresoCola(cliente), cantProductos <- (cantProductos(cliente) - 1)))
}

# proposito: retorna la caja de la linea de cajas cuyo numero es nroCaja
# precondicion: hay una caja identificada por nroCaja
function buscarCaja(lineaDeCajas, nroCaja) {
	laux := lineaDeCajas
	while (tieneAlgo(laux) && (numero(head(laux)) /= nroCaja)) {
		laux := tail(laux)
	}
	return(head(laux))
}
