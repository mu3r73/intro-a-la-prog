function longitud(lista) {
    recorrido := lista
    cantidad := 0

    while (not isEmpty(recorrido)) {
        cantidad := cantidad + 1
        recorrido := tail(recorrido)
    }

    return (cantidad)
}
