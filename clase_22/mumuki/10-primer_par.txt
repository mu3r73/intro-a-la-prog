function primerPar(lista) {
    laux := lista
    
    while (not isEmpty(laux) && not esPar(head(laux))) {
        laux := tail(laux)
    }
    
    return(head(laux))
}
